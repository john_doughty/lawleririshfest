<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<p>
<?php

	include 'main.php';
	$main = new Main;
	$connection = $main->ConnectionToDatabase();

		$shiftdata = $connection->query(
			"SELECT v.id, v.email, v.first_name, v.last_name, l.name, UNIX_TIMESTAMP(st.start_time) as start_time, UNIX_TIMESTAMP(st.end_time) as end_time
		FROM volunteer v 
		JOIN volunteer_obligation_list o 
			ON v.id = o.vid
		JOIN shift s
			ON o.sid =  s.id
		JOIN shift_time st
			ON st.id = s.shift_time_id
		JOIN locations l
			ON l.id = s.location_id
		ORDER BY v.last_name, v.first_name, o.vid;");  

		// foreach ($locationsdata as $key => $value) {

		// 	print_r($value);

		// 	print("<br/>");

		// }
		$volunteer = null;
		$volunteers = array();

		$volunteerLength=0;

		foreach ($shiftdata as $key => $value) {
			// print($value['first_name']);
			// if(isset($volunteer->firstName))
			// {
			// 	print $volunteer->firstName." ".$value['first_name']."\n";
			// }
			if($volunteer == null 
				|| 
				$volunteer->firstName != $value['first_name'] || 
				$volunteer->lastName != $value['last_name'] ||
				$volunteer->email != $value['email'])
			{
				if($volunteer != null)
				{
					array_push($volunteers,$volunteer);
				}
				$volunteer = new stdClass();
				$volunteer->id = $value['id'];
				$volunteer->email = $value['email'];
				$volunteer->firstName = $value['first_name'];
				$volunteer->lastName = $value['last_name'];
				$volunteer->locations[] = [$value['name'], date('g:i M jS',$value['start_time']), date('g:i M jS',$value['end_time'])];
				$volunteerLength++;
			}
			else
			{
				array_push($volunteer->locations,  [$value['name'], $value['start_time'], $value['end_time']]);
			}
		}
		if($volunteer != null)
		{//Add the last one
			array_push($volunteers,$volunteer);
		}

		for($i=0;$i<$volunteerLength;$i++)
		{
			$to = $volunteers[$i]->email;
		 	$headers = "From: no-reply@lawleririshfest.com";
		 	$subject = "Lawler Irish Fest Volunteer times";
		 	$emailString = "";
		 	for($j=0;$j<count($volunteer->locations);$j++)
		 	{
		 		$emailString = $emailString.$volunteers[$i]->locations[$j][0]." ".$volunteers[$i]->locations[$j][1]." - ".$volunteers[$i]->locations[$j][2]."\n";
		 	}
		 	print $emailString;
		 	$body = $volunteers[$i]->firstName." ".$volunteers[$i]->lastName.",\n We have you down for ".count($volunteer->locations)." volunteer shifts.
		 	\nThese shifts are:\n".$emailString." 
		 	\n\nFor more info, copy this into your address bar: lawleririshfest.com/grab.php?id=".$volunteers[$i]->id;
			try
			{
				if (true)//mail($to, $subject, $body,$headers)) 
				{
					print("script broken to prevent hacking");
					print($volunteers[$i]->firstName." ".$volunteers[$i]->lastName."\n ".$to."\n ".$subject."\n ".$body."\n ".$headers);
				} 
				else 
				{
				}
			}	
			catch (Exception $e)
			{
				throw new Exception( 'Something really gone wrong', 0, $e);
			}	
		}
?>
</p>
</body>
</html>