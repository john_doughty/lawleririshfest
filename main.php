<?php
	// Connect to Database 
	class Main
	{
		public function ConnectionToDatabase()
		{
			try {
			    $conn = new PDO('mysql:host=localhost;dbname=irishfest', 'irishfest', 'Luck2013OfTheIrish');
			    $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			    return $conn;
			} catch(PDOException $e) {
			    echo 'ERROR: ' . $e->getMessage();
			    echo $conn->errorCode();
				echo $conn->errorInfo();
				return null;
			}
		}
	}

	class Location
	{
		private $locationID;
		private $locationName;
		private $count;
		private $shiftArray;

		function AddShift($shift)//WE ARE NOT GOING TO EVER REMOVE SHIFTS!!! This system is effecient and effective because of this fact
		{
			$this->shiftArray[$this->count] = $shift;
			$this->count++;
		}

		function GetNumShifts()
		{
			return $this->count;
		}

		function GetShifts()
		{
			return $this->shiftArray;
		}

		function Location($locationID,$locationName)
		{
			$this->locationID = $locationID;
			$this->locationName = $locationName;
			$this->count=0;
		}

		function GetLocationName()
		{
			return $this->locationName;
		}

		function GetlocationID()
		{
			return $this->locationID;
		}
	}

	class GrabVolunteer //extends Volunteer
	{
		private $startTime;
		private $endTime;
		private $firstName;
		private $lastName;
		private $email;
		private $phone;
		private $location;

		function GrabVolunteer($firstName,$lastName,$email,$phone,$location,$startTime,$endTime)
		{
			$this->firstName = $firstName;
			$this->lastName = $lastName;
			$this->email = $email;
			$this->phone = $phone;
			$this->location = $location;
			$this->startTime = date('g:i jS',$startTime);
			$this->endTime = date('g:i jS',$endTime);
		}

		function GetFirstName()
		{
			return $this->firstName;
		}

		function GetLastName()
		{
			return $this->lastName;
		}

		function GetEmailName()
		{
			return $this->email;
		}

		function GetPhone()
		{
			return $this->phone;
		}

		function GetLocation()
		{
			return $this->location;
		}

		function GetStartTime()
		{
			return $this->startTime;
		}

		function GetEndTime()
		{
			return $this->endTime;
		}
	}

	class Volunteer
	{
		private $firstName;
		private $lastName;
		private $email;
		private $phone;
		private $location;

		function Volunteer($firstName,$lastName,$email,$phone,$location)
		{
			$this->firstName = $firstName;
			$this->lastName = $lastName;
			$this->email = $email;
			$this->phone = $phone;
			$this->location = $location;
		}

		function GetFirstName()
		{
			return $this->firstName;
		}

		function GetLastName()
		{
			return $this->lastName;
		}

		function GetEmailName()
		{
			return $this->email;
		}

		function GetPhone()
		{
			return $this->phone;
		}

		function GetLocation()
		{
			return $this->location;
		}

	}

				

	class ShiftTime
	{
		private $shifttimeID;
		private $dayOfTheWeek;
		private $startTime;
		private $date;
		private $endTime;

		// function ShiftTime($shifttimeID,$startTime,$endTime)
		// {
		// 	//print($shifttimeID);
		// 	$this->shifttimeID = $shifttimeID;
		// 	$this->startTime = date('g:i',$startTime);
		// 	$this->date = date('M j, Y',$startTime);
		// 	$this->dayOfTheWeek = date('l',$startTime);
		// 	$this->endTime = date('g:i',$endTime);
		// }
		function ShiftTime($shifttimeID,$startTime,$endTime,$date,$dayOfTheWeek)
		{
			//print($shifttimeID);
			$this->shifttimeID = $shifttimeID;
			$this->startTime = $startTime;
			$this->endTime = $endTime;
			$this->date = $date;
			$this->dayOfTheWeek = $dayOfTheWeek;
		}

		function GetID()
		{
			return $this->shifttimeID;
		}

		function GetStartTime()
		{
				return $this->startTime;
		}

		function GetDate()
		{
				return $this->date;
		}

		function GetEndTime()
		{
				return $this->endTime;
		}
		function GetDayOfTheWeek()
		{
				return $this->dayOfTheWeek;
		}
	}

	class Shift
	{
		private $shiftID;
		private $enrolledCount;
		private $maxToEnroll;
		private $timeID;
		private $locationName;
		private $shifttimeID;
		private $dayOfTheWeek;
		private $startTime;
		private $date;
		private $endTime;


		
		// function Shift($shiftID,$locationID,$timeID,$enrolledCount,$maxToEnroll)
		// {
		// 	$this->shiftID = $shiftID;
		// 	$this->locationID = $locationID;
		// 	$this->timeID = $timeID;
		// 	$this->enrolledCount = $enrolledCount;
		// 	$this->maxToEnroll = $maxToEnroll;
		// }

		function Shift($locationName,$shiftID,$timeID,$enrolledCount,$maxToEnroll,$startTime,$endTime)
		{

			$this->shiftID 			= $shiftID;
			$this->locationName 	= $locationName;
			$this->timeID 			= $timeID;
			$this->enrolledCount 	= $enrolledCount;
			$this->maxToEnroll 		= $maxToEnroll;
			$this->startTime 		= date('g:i',$startTime);
			$this->date 			= date('M j, Y',$startTime);
			$this->dayOfTheWeek 	= date('l',$startTime);
			$this->endTime 			= date('g:i',$endTime);
		}

		function GetLocationName()
		{
			return $this->locationName;
		}

		function GetTimeID()
		{
			return $this->timeID;
		}

		function GetID()
		{
			return $this->shiftID;
		}

		function GetEnrollCount()
		{
				return $this->enrolledCount;
		}

		function GetMaxEnrollCount()
		{
				return $this->maxToEnroll;
		}
		// function GetTimeID()
		// {
		// 	return $this->shifttimeID;
		// }

		function GetStartTime()
		{
				return $this->startTime;
		}

		function GetDate()
		{
				return $this->date;
		}

		function GetEndTime()
		{
				return $this->endTime;
		}
		function GetDayOfTheWeek()
		{
				return $this->dayOfTheWeek;
		}
	}

?>
