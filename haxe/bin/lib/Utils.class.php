<?php

class Utils {
	public function __construct() { 
	}
	static function GetDayString($dayInt) {
		$day = null;
		switch($dayInt) {
		case 0:{
			$day = "Sunday";
		}break;
		case 1:{
			$day = "Monday";
		}break;
		case 2:{
			$day = "Tuesday";
		}break;
		case 3:{
			$day = "Wednesday";
		}break;
		case 4:{
			$day = "Thursday";
		}break;
		case 5:{
			$day = "Friday";
		}break;
		case 6:{
			$day = "Saturday";
		}break;
		default:{
			$day = "Invalid";
		}break;
		}
		return $day;
	}
	static function GetAbrviatedMonth($monthInt) {
		$monthString = null;
		switch($monthInt) {
		case 1:{
			$monthString = "Jan";
		}break;
		case 2:{
			$monthString = "Feb";
		}break;
		case 3:{
			$monthString = "Mar";
		}break;
		case 4:{
			$monthString = "Apr";
		}break;
		case 5:{
			$monthString = "May";
		}break;
		case 6:{
			$monthString = "June";
		}break;
		case 7:{
			$monthString = "July";
		}break;
		case 8:{
			$monthString = "Aug";
		}break;
		case 9:{
			$monthString = "Sep";
		}break;
		case 10:{
			$monthString = "Oct";
		}break;
		case 11:{
			$monthString = "Nov";
		}break;
		case 12:{
			$monthString = "Dec";
		}break;
		default:{
			$monthString = "Inv";
		}break;
		}
		return $monthString;
	}
	static function GetAbrviatedName($monthInt) {
		$monthString = null;
		switch($monthInt) {
		case 1:{
			$monthString = "January";
		}break;
		case 2:{
			$monthString = "February";
		}break;
		case 3:{
			$monthString = "March";
		}break;
		case 4:{
			$monthString = "April";
		}break;
		case 5:{
			$monthString = "May";
		}break;
		case 6:{
			$monthString = "June";
		}break;
		case 7:{
			$monthString = "July";
		}break;
		case 8:{
			$monthString = "August";
		}break;
		case 9:{
			$monthString = "September";
		}break;
		case 10:{
			$monthString = "October";
		}break;
		case 11:{
			$monthString = "November";
		}break;
		case 12:{
			$monthString = "December";
		}break;
		default:{
			$monthString = "Invalid month";
		}break;
		}
		return $monthString;
	}
	static function GetNonMilitaryHours($hours) {
		if(_hx_mod($hours, 12) === 0) {
			return 12;
		} else {
			return _hx_mod($hours, 12);
		}
	}
	static function GetDoubleDigitMinutes($mins) {
		if($mins < 10) {
			return "0" . _hx_string_rec($mins, "");
		} else {
			return $mins;
		}
	}
	function __toString() { return 'Utils'; }
}
