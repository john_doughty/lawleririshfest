<?php

class Location {
	public function __construct($locationID, $locationName) {
		if(!php_Boot::$skip_constructor) {
		$this->locationID = $locationID;
		$this->locationName = $locationName;
		$this->count = 0;
	}}
	public $locationID;
	public $locationName;
	public $count;
	public $shiftArray;
	public function AddShift($shift) {
		$this->shiftArray[$this->count] = $shift;
		$this->count++;
	}
	public function GetNumShifts() {
		return $this->count;
	}
	public function GetShifts() {
		return $this->shiftArray;
	}
	public function GetLocationName() {
		return $this->locationName;
	}
	public function GetlocationID() {
		return $this->locationID;
	}
	public function __call($m, $a) {
		if(isset($this->$m) && is_callable($this->$m))
			return call_user_func_array($this->$m, $a);
		else if(isset($this->__dynamics[$m]) && is_callable($this->__dynamics[$m]))
			return call_user_func_array($this->__dynamics[$m], $a);
		else if('toString' == $m)
			return $this->__toString();
		else
			throw new HException('Unable to call <'.$m.'>');
	}
	function __toString() { return 'Location'; }
}
