package ;

/**
 * ...
 * @author John Doughty
 */
class Shift
{
	private var shiftID:Int;
	private var enrolledCount:Int;
	private var maxToEnroll:Int;
	private var timeID:Int;
	private var locationName:String;
	private var shifttimeID:Int;
	private var dayOfTheWeek:String;
	private var startTime:String;
	private var date:String;
	private var endTime:String;
	
	public function new(locationName,shiftID,timeID,enrolledCount,maxToEnroll,startTime,endTime) 
	{
		var minutes:String;
		
		this.shiftID = shiftID;
		this.locationName 	= locationName;
		this.timeID = timeID;
		this.enrolledCount = enrolledCount;
		this.maxToEnroll = maxToEnroll;
		
		this.date = untyped __call__('date','M j, Y', startTime);
		this.dayOfTheWeek = untyped __call__('date','l', startTime);
		this.startTime = untyped __call__('date','g:i', startTime);
		this.endTime = untyped __call__('date','g:i', endTime);
	}
	

	public function GetLocationName()
	{
		return this.locationName;
	}

	public function GetTimeID()
	{
		return this.timeID;
	}

	public function GetID()
	{
		return this.shiftID;
	}

	public function GetEnrollCount()
	{
			return this.enrolledCount;
	}

	public function GetMaxEnrollCount()
	{
			return this.maxToEnroll;
	}

	public function GetStartTime()
	{
			return this.startTime;
	}

	public function GetDate()
	{
			return this.date;
	}

	public function GetEndTime()
	{
			return this.endTime;
	}
	public function GetDayOfTheWeek()
	{
			return this.dayOfTheWeek;
	}
}