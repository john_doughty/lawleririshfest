package ;

/**
 * ...
 * @author John Doughty
 */
class Volunteer
{
	private var firstName:String;
	private var lastName:String;
	private var email:String;
	private var phone:String;
	private var location:String;

	public function new(firstName:String,lastName:String,email:String,phone:String,location:String)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.location = location;
	}

	public function GetFirstName()
	{
		return this.firstName;
	}

	public function GetLastName()
	{
		return this.lastName;
	}

	public function GetEmailName()
	{
		return this.email;
	}

	public function GetPhone()
	{
		return this.phone;
	}

	public function GetLocation()
	{
		return this.location;
	}

}