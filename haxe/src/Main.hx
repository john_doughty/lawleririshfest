package ;

import php.Lib;
import php.Web;
import sys.db.ResultSet;

/**
 * ...
 * @author John Doughty
 */

class Main 
{
	private var dbManager:DBManager;
	static function main() 
	{
		new Main();
	}
	
	public function new()
	{
		var data = Web.getParams();
		dbManager = new DBManager();
		//trace(data);
		if (data.get("submit") != null)
		{
			new Submit(dbManager);
		}
		else if (data.get("grab") != null)
		{
			new Grab(dbManager);
		}
		else {
			new Form(dbManager);
		}
	}
}