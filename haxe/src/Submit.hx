package ;

import haxe.ds.ObjectMap;
import php.Lib;
import php.Lib.print;
import php.Web;
import sys.db.ResultSet;
/**
 * ...
 * @author John Doughty
 */
class Submit  extends Page
{

		var shiftCount:Int=0;
		var volunteerID:Int;
		var emailString:String;
		// Connect to Database 
	public function new(dbManager:DBManager)
	{
		super();
		var data:ResultSet;
		var shiftID:Int;
		var ids:String = "";
		var noNumber:Bool = false;
		var noFirstName:Bool = false;
		var noLastName:Bool = false;
		var noeMail:Bool = false;
		var shiftsSignedUpFor:Array<Dynamic> = new Array<Dynamic>();
		var number:String = "";
		var missingFieldsStr:String = "";
		var i:Int;
		var castableshifts:Dynamic = Web.getParams().get('shift');
		var shifts:Array<Int> = cast(castableshifts);
		dbManager.Open();
		data = dbManager.GetResults('SELECT COUNT(id) as count FROM volunteer');
		
		for (result in data) 
		{
			volunteerID = result.count;
		}
		
		if(cast(Web.getParams().get('number'),String)=='')
		{
			noNumber = true;
			missingFieldsStr += 'number=' + noNumber;
		}

		if(cast(Web.getParams().get('firstName'),String)=='')
		{
			noFirstName = true;
			if(missingFieldsStr !='')
				missingFieldsStr += '&';
			missingFieldsStr += 'firstName=' + noFirstName;
		}

		if(cast(Web.getParams().get('lastName'),String)=='')
		{
			noLastName = true;
			if(missingFieldsStr !='')
				missingFieldsStr += '&';
			missingFieldsStr += 'lastName=' + noLastName;
		}

		if(cast(Web.getParams().get('Email'),String)=='')
		{
			noeMail = true;
			if(missingFieldsStr !='')
				missingFieldsStr += '&';
			missingFieldsStr += 'Email=' + noeMail;
			
		}

		if(noeMail || noLastName ||noFirstName ||noNumber|| untyped __call__('count',shifts) == 0)
		{
			print('<script type="text/javascript">' +
				'window.location.replace("http://localhost/index.php?'+ missingFieldsStr +'");' +
			'</script>');
			untyped __call__('die');
		}
		
		number = cast(Web.getParams().get('number'),String);
		number = StringTools.replace(number,"(", "");
		number = StringTools.replace(number,")", "");
		number = StringTools.replace(number,"-", "");
		//taken from http://stackoverflow.com/questions/6040367/php-adding-characters-in-middle-of-a-string-starting-from-the-end
		number = untyped __call__('preg_replace','/^(.*?)(.{3})(.{4})$/', '$1-$2-$3', number);
		dbManager.GetResults('INSERT INTO volunteer(id,first_name,last_name,email,phone)VALUES(	' + volunteerID + ',"' + Web.getParams().get('firstName') + '","' + Web.getParams().get('lastName') + '","' + Web.getParams().get('Email') + '","' + number + '")');
		
		for (i in 0...untyped __call__('count',shifts))
		{
			shiftID = shifts[i];
			if (ids != "")
				ids=ids+",";
			ids=ids+shifts[i];
			dbManager.GetResults('INSERT INTO volunteer_obligation_list (vid,sid) VALUES(' + volunteerID + ',' + shiftID + ');');
			shiftCount++;
		}
		dbManager.GetResults('UPDATE shift SET volunteers = volunteers + 1 WHERE  id IN ("'+ids+'");');
		data = dbManager.GetResults('SELECT l.name as location,UNIX_TIMESTAMP(st.start_time) as start_time,UNIX_TIMESTAMP(st.start_time) as end_time FROM shift s JOIN shift_time st ON st.id = s.shift_time_id JOIN locations l ON l.id = s.location_id WHERE s.id IN ("'+ids+'") ORDER BY s.shift_time_id;');
		i = 0;
		for (row in data) {
			shiftsSignedUpFor.push({});
			shiftsSignedUpFor[i].name =  row.location;
			shiftsSignedUpFor[i].startTime  =  untyped __call__('date','g:i M j, Y', row.start_time);
			shiftsSignedUpFor[i].endTime  =  untyped __call__('date','g:i M j, Y', row.end_time);
			i++;
		}
		for(i in 0...shiftCount) {
			emailString=emailString+"\n"+shiftsSignedUpFor[i].name+" at "+shiftsSignedUpFor[i].startTime;
		}
		new HeaderHTML();
		buildMainContent();
		sendEmail();
	}
	private function buildMainContent()
	{
		print(	'<div id="main-content">' +
					'<div id="title">' +
						'Thanks for Volunteering!<br/>' +
						'We\'ll see you there!' +
					'</div>' +
					'<div>' +
						'<br/>' +
						'<br/>' +
						'<br/>' +
						'Check your email for a confirmation message!' +
						'<br/>' +
						'If you don\'t see it check your spam.' +
						'<br/>' +
						'Please wait while we redirect you back to our main page...' +
					'</div>' +
					'<script type="text/javascript">' +
						'setTimeout(function(){window.location.replace("http://www.lawleririshfest.com")},5000);' +
					'</script>'+
				'</div>');
	}
	
	private function sendEmail()
	{
		var to:String = "irishfestvolunteers@gmail.com";
	 	var headers:String = "From: no-reply@lawleririshfest.com";
	 	var subject:String = /*$_POST*/Web.getParams().get('firstName')+" "+/*$_POST*/Web.getParams().get('lastName')+" has volunteered";
	 	var body:String = /*$_POST*/Web.getParams().get('firstName')+' '+/*$_POST*/Web.getParams().get('lastName')+' has volunteered for '+shiftCount+' positions.'+
	 	'\nThese Positions are:'+emailString+ 
	 	'\n\nFor more info, copy this into your address bar: lawleririshfest.com/grab.php?id='+volunteerID;
	
		if (Lib.mail(to, subject, body,headers)) {
		} else {
		}
		if(Web.getParams().get('Email') != null)
		{
			if(cast(Web.getParams().get('Email'),String)!="")
			{
				to = cast(Web.getParams().get('Email'),String);
			 	headers = 'From: no-reply@lawleririshfest.com';
			 	subject = 'Thank you for volunteering!';
			 	body = 'Thank you for volunteering for ' + shiftCount + ' positions.' +
	 			'\nThese Positions are:'+emailString+
			 	'\n\nThank you';
				
				if (Lib.mail(to, subject, body, headers)) 
				{
				} 
				else 
				{
				}
			}
		}
	}
}