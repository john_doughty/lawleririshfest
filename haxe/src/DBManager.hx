package ;


import php.db.PDO;
import sys.db.Connection;
import sys.db.ResultSet;

/**
 * ...
 * @author John Doughty
 */
class DBManager
{
	public var conn:Connection;
	public static var DSN:String = 'mysql:host=localhost;dbname=irishfest';
	public static var USERNAME:String = 'root';
	public static var PASSWORD:String = '';
	
	public function new() 
	{
		
	}
	public function Open() 
	{
		conn = PDO.open(DBManager.DSN, DBManager.USERNAME, DBManager.PASSWORD);//,untyped __php__("array(PDO::ATTR_EMULATE_PREPARES => false,PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)"));
	}
	public function GetResults(sqlString:String):ResultSet
	{
		return conn.request(sqlString);
	}
	public function Close()
	{
		conn.close();
	}
}