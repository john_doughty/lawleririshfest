package ;

/**
 * ...
 * @author John Doughty
 */
class ShiftTime
{
	private var shifttimeID:Int;
	private var dayOfTheWeek:String;
	private var startTime:String;
	private var date:String;
	private var endTime:String;
	
	public function new(shifttimeID:Int,startTime:String,endTime:String,date:String,dayOfTheWeek:String)
	{
		this.shifttimeID = shifttimeID;
		this.startTime = startTime;
		this.endTime = endTime;
		this.date = date;
		this.dayOfTheWeek = dayOfTheWeek;
	}

	public function GetID()
	{
		return this.shifttimeID;
	}

	public function GetStartTime()
	{
		return this.startTime;
	}

	public function GetDate()
	{
		return this.date;
	}

	public function GetEndTime()
	{
		return this.endTime;
	}
	public function GetDayOfTheWeek()
	{
		return this.dayOfTheWeek;
	}
}