package ;

/**
 * ...
 * @author John Doughty
 */
class Utils
{

	public function new() 
	{
		
	}
	
	public static function GetDayString(dayInt:Int)
	{
		var day: String;
		switch(dayInt)
		{
		   case 0:
				day = "Sunday"; 
			case 1:
				day = "Monday"; 
			case 2:
				day = "Tuesday"; 
			case 3:
				day = "Wednesday"; 
			case 4:
				day = "Thursday"; 
			case 5:
				day = "Friday"; 
			case 6:
				day = "Saturday"; 
			default:
				day = "Invalid";
					
		}
		return day;
	}
	
	public static function GetAbrviatedMonth(monthInt:Int)
	{
		var monthString:String;
		switch (monthInt) {
            case 1:  monthString = "Jan";
            case 2:  monthString = "Feb";
            case 3:  monthString = "Mar";
            case 4:  monthString = "Apr";
            case 5:  monthString = "May";
            case 6:  monthString = "June";
            case 7:  monthString = "July";
            case 8:  monthString = "Aug";
            case 9:  monthString = "Sep";
            case 10: monthString = "Oct";
            case 11: monthString = "Nov";
            case 12: monthString = "Dec";
            default: monthString = "Inv";
        }
		return monthString;
	}
	public static function GetAbrviatedName(monthInt:Int)
	{
		var monthString:String;
		switch (monthInt) {
            case 1:  monthString = "January"; 
            case 2:  monthString = "February"; 
            case 3:  monthString = "March"; 
            case 4:  monthString = "April"; 
            case 5:  monthString = "May"; 
            case 6:  monthString = "June"; 
            case 7:  monthString = "July"; 
            case 8:  monthString = "August"; 
            case 9:  monthString = "September"; 
            case 10: monthString = "October"; 
            case 11: monthString = "November"; 
            case 12: monthString = "December"; 
            default: monthString = "Invalid month"; 
        }
		return monthString;
	}
	
	public static function GetNonMilitaryHours(hours:Int)
	{
		if (hours % 12 == 0)
			return 12;
		else
			return hours % 12;
	}
	public static function GetDoubleDigitMinutes(mins:Int)
	{
		if (mins < 10)
		{
			return "0" + mins;
		}
		else
		{
			return cast(mins,String);
		}
	}
}