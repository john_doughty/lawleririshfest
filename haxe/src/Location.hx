package ;

/**
 * ...
 * @author John Doughty
 */
class Location
{

	private var locationID:Int;
	private var locationName:String;
	private var count:Int;
	private var shiftArray:Array<Shift>;
	
	public function new(locationID:Int, locationName:String) 
	{
			this.locationID = locationID;
			this.locationName = locationName;
			this.count = 0;
	}

	public function AddShift(shift)
	{
		this.shiftArray[this.count] = shift;
		this.count++;
	}

	public function GetNumShifts()
	{
		return this.count;
	}

	public function GetShifts()
	{
		return this.shiftArray;
	}

	public function GetLocationName()
	{
		return this.locationName;
	}

	public function GetlocationID()
	{
		return this.locationID;
	}
}