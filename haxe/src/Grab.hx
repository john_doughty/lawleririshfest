package ;

import php.Lib.print;
import php.Web;
/**
 * ...
 * @author John Doughty
 */
class Grab
{

	private var searchingByID:Bool;
	private var first:Bool;
	private var last:Bool;
	private var number:Bool;
	private var email:Bool;
	private var location:Bool;
		
	public function new(dbManager:DBManager) 
	{

		new HeaderHTML();
		
		searchingByID = Web.getParams().get("id") != null && Web.getParams().get("id") != "";
		first = Web.getParams().get("firstName") != null && Web.getParams().get("firstName") != "";
		last = Web.getParams().get("lastName") != null && Web.getParams().get("lastName") != "";
		number = Web.getParams().get("number") != null && Web.getParams().get("number") != "";
		email = Web.getParams().get("Email") != null && Web.getParams().get("Email") != "";
		location = Web.getParams().get('location') != null && Web.getParams().get("location") != "";
		

		dbManager.Open();
		
		//print($queryString);
		var data = dbManager.GetResults(constructQueryString());

		var volunteers:Array<GrabVolunteer>  = [];
		var i = 0;

		for (result in data) //start here
		{
			volunteers[i] = new GrabVolunteer
			(
				result.first_name,
				result.last_name,
				result.email,
				result.phone,
				result.location,
				result.start_time,
				result.end_time
			);
			i++;
		}
		var volunteersLength = i;

		data = dbManager.GetResults('SELECT name FROM locations');
		dbManager.Close();
		i = 0;
		var locations = [];
		for (result in data) 
		{
			locations[i] = result.name;
			i++;
		}
		var locationsLength = i;

		print('<form id="grabForm" action ="index.php?grab=1" method = "post">');
		print('<table cellpadding="3">');
		print('<tr>');
		print('<td >');
		print("First Name");
		print('</td>');
		print('<td>');
		print("Last Name");
		print('</td>');
		print('<td>');
		print("Email Address");
		print('</td>');
		print('<td>');
		print("Phone Number");
		print('</td>');
		print('<td>');
		print("Start Time");
		print('</td>');
		print('<td>');
		print("End Time");
		print('</td>');
		print('<td>');
		print("Location");
		print('</td>');
		print('</tr>');
		print('<tr>');
		print('<td >');
		print('<input type="text" name="firstName">');
		print('</td>');
		print('<td>');
		print('<input type="text" name="lastName">');
		print('</td>');
		print('<td>');
		print('<input type="text" name="eMail">');
		print('</td>');
		print('<td>');
		print('<input type="text" name="number">');
		print('</td>');
		print('<td/>');
		print('<td/>');
		print('<td>');

		for(i in 0...locationsLength)
		{
			print('<br/><input type="radio" name="location" value="'+locations[i]+'">'+locations[i]);
		}
		print('</td>');
		print('<td>');
		print('<input style = "height: 25px;width:50px" type="submit" value="Filter">');
		print('</td>');
		print('</tr>');
		for(i in 0...volunteersLength) 
		{
			print ("<tr "+(i%2==0?"class='darkRow'":"class='lightRow'")+">
				<td>"+volunteers[i].GetFirstName()+"</td>
				<td>"+volunteers[i].GetLastName()+"</td>
				<td>"+volunteers[i].GetEmailName()+"</td>
				<td>"+volunteers[i].GetPhone()+"</td>
				<td>"+volunteers[i].GetStartTime()+"</td>
				<td>"+volunteers[i].GetEndTime()+"</td>
				<td>"+volunteers[i].GetLocation()+"</td>
				</tr>");
		}
		print('</table>');
		print("</form>");
	}
	
	
	private function constructQueryString(): String
	{
		var queryString = "SELECT DISTINCT v.first_name, v.last_name,v.email,
		 v.phone, l.name as location,
		 UNIX_TIMESTAMP(st.start_time) as start_time,
		 UNIX_TIMESTAMP(st.end_time) as end_time 
			FROM volunteer v
			JOIN volunteer_obligation_list vl ON v.id = vl.vid
			JOIN shift s ON s.id = vl.sid
			JOIN shift_time st ON st.id = s.shift_time_id
			JOIN locations l ON l.id = s.location_id";

		if(first||last||email||number||location||searchingByID)
		{
			queryString = queryString+" WHERE ";
		}

		if(first)
		{
			queryString = queryString+"v.first_name = '"+/*$_POST*/Web.getParams().get('firstName')+"'" ;
		}

		if(last)
		{
			if(first)
				queryString = queryString+" AND ";
			queryString = queryString+"v.last_name = '"+/*$_POST*/Web.getParams().get('lastName')+"'" ;
		}
		
		if(email)
		{
			if(first||last)
				queryString = queryString+" AND ";
			queryString = queryString+"v.email = '"+/*$_POST*/Web.getParams().get('eMail')+"'" ;
		}
		
		if(number)
		{
			if(first||last||email)
				queryString = queryString+" AND ";
			queryString = queryString+"v.phone = '"+/*$_POST*/Web.getParams().get('number')+"'" ;
		}

		if(location)
		{
			if(first||last||email||number)
				queryString = queryString+" AND ";
			queryString = queryString+"l.name = '"+/*$_POST*/Web.getParams().get('location')+"'" ;
		}

		if(searchingByID)
		{
			if(first||last||email||number||location)
				queryString = queryString+" AND ";
			queryString = queryString+"v.id = '"+/*$_GET*/Web.getParams().get('id')+"'" ;
		}

		return queryString = queryString+" ORDER BY st.start_time,l.name,v.last_name,v.first_name ASC";
	}
	
}