package ;

//import Shift;
import ShiftTime;
import DBManager;
import sys.db.ResultSet;
import php.Web;
import php.Lib.print;
/**
 * ...
 * @author John Doughty
 */
class Form extends Page
{	
	private var shifts:Array<Shift> = [];
	private var locations:Array<String> = [];
	private var shiftTimes:Array<ShiftTime> = [];
	
	private var shiftsLength:Int = 0;
	private var locationLength:Int = 0;
	private var shiftTimeLength:Int = 0;
	
	private var noNumber:Bool;
	private var noFirstName:Bool;
	private var noLastName:Bool;
	private var noEmail:Bool;
	
	public function new(dbManager:DBManager)
	{
		super();
		new HeaderHTML();
		checkGET();
		getData(dbManager);
		printMainContent();
	}
	
	private function getData(dbManager:DBManager) 
	{
		var i:Int = 0;
		var j:Int = 0;
		var duplicateLocation:Bool = false;
		var duplicateShiftTime:Bool = false;
		var locationResults:ResultSet;
		
		dbManager.Open();
		locationResults = dbManager.GetResults('SELECT l.name as location,s.id,s.shift_time_id,(SELECT COUNT(*) FROM volunteer_obligation_list WHERE sid = s.id) as volunteers, s.max_volunteers,UNIX_TIMESTAMP(st.start_time) as start_time,UNIX_TIMESTAMP(st.end_time) as end_time FROM shift s JOIN shift_time st ON st.id = s.shift_time_id	JOIN locations l ON l.id = s.location_id LEFT JOIN volunteer_obligation_list v ON s.id = v.sid ORDER BY st.start_time;');
		for (result in locationResults)
		{
			shifts[i] = new Shift(result.location, 
			result.id, 
			result.shift_time_id, 
			result.volunteers, 
			result.max_volunteers, 
			result.start_time, 
			result.end_time);
			i++;
		}
		shiftsLength = i;
		for (i in 0...shiftsLength)
		{
			duplicateLocation = false;
			duplicateShiftTime = false;
			
			for (j in 0...locationLength)
			{
				if (locations[j] == shifts[i].GetLocationName())
				{
					duplicateLocation = true;
					break;
				}
			}
			
			for (j in 0...shiftTimeLength)
			{
				if (shiftTimes[j].GetID() == shifts[i].GetTimeID())
				{
					duplicateShiftTime = true;
					break;
				}
			}
			if (!duplicateLocation)
			{
				locations[locationLength] = shifts[i].GetLocationName();
				locationLength++;
			}
			
			if (!duplicateShiftTime)
			{
				shiftTimes[shiftTimeLength] = new ShiftTime(shifts[i].GetTimeID(),shifts[i].GetStartTime(),shifts[i].GetEndTime(),shifts[i].GetDate(),shifts[i].GetDayOfTheWeek());
				shiftTimeLength++;
			}
		}	
	}
	
	private function checkGET()
	{
		noNumber = Web.getParams().get("firstName") != null;
		noFirstName = Web.getParams().get("lastName") != null;
		noLastName = Web.getParams().get("number") != null;
		noEmail = Web.getParams().get("Email") != null;
	}
	
	private function printMainContent()
	{
		print('<div id="main-content">'+
			'<div id="title">'+
				'Lawler Irish Festival <br/>Volunteer Form'+
			'</div>');
		buildForm();
	}
	private function buildForm()
	{
		print(
			'<form id="volunteerForm" action ="index.php?submit=1" method = "post">'+
				'<table>'+
					'<div class="formElement">');
		buildInput('First Name', 'firstName', noFirstName);
		buildInput('Last Name', 'lastName', noLastName);
		buildInput('Email Address', 'Email', noEmail);
		buildInput('Phone Number', 'number', noNumber);
		print(	'<tr>'+
					'<td/>'+
					'<td colspan="' + locationLength + '" style="vertical-align: middle;">' + 
						'<div>NOTE: The two numbers in each column (0/1) represent the number of people who have volunteered, followed by the number of volunteers needed for each position. A position with a * requires being 21 years of age.</div>' +
					'</td>'+
				'</tr>' +
			'</div>' +
		'<div class="formElement">');
		buildCheckArea();
		buildEnd();
	}
	
	private function buildInput(title:String,name:String, boolToCheck:Bool)
	{
		print(
			'<tr>'+
				'<td>' +
					'<div class="column1">' + title+'</div>' +
				'</td>' +
				'<td colspan="' + locationLength + '" style="vertical-align: middle;">'+
					'<div >' +
						'<input type="text"');
		if (boolToCheck){
			print(			'style="border: 5px solid red;"');
		}
		print(			'name="'+name+'">' +
					'</div>' +
				'<td>'+
			'</tr>');
	}
	
	private function buildCheckArea()//Needs rewritten
	{
		var dayoftheweek:String = "";
		var lastDate:String = "";
		var bDark:Bool;
		var i:Int;
		var j:Int;
		var k:Int;
		var l:Int;
		var havePlacedCheckBox:Bool;
		
		print('<div class="checkArea">');

		for(i in 0...shiftTimeLength)
		{
			if(shiftTimes[i].GetDate() != lastDate)
			{
				print('<tr><td/>');
				bDark=true;

				for(l in 0...locationLength)
				{
					print('<td '+(bDark?'style = "background-color: #a4b193;"':'')+'><div class="checkHeader" >'+locations[l]+'</div></td>');
					bDark ? bDark = false : bDark = true;
				}

				print('</tr><tr><td class="day">' + shiftTimes[i].GetDayOfTheWeek() + '</td>');
				
				for(l in 0...locationLength)
				{
					print('<td '+(bDark?'style = "background-color: #a4b193;height:32px;"':'')+'><div class="checkHeader" ></div></td>');
					bDark ? bDark = false : bDark = true;
				}
				print('</tr>');
			}
			print('<tr><td><div class="time">'+shiftTimes[i].GetStartTime()+' - '+shiftTimes[i].GetEndTime()+'</div></td>');

			bDark=true;
			for(j in 0...locationLength)
			{
				print("<td><div "+(bDark?"class='darkColumn'":"class='lightColumn'")+">");
				bDark ? bDark = false : bDark = true;
				havePlacedCheckBox = false;

				for(k in 0...shiftsLength)
				{
					if(shifts[k].GetLocationName()==locations[j] && shifts[k].GetTimeID()==shiftTimes[i].GetID())
					{
						if(shifts[k].GetEnrollCount() >=shifts[k].GetMaxEnrollCount())
						{
							print('<input class="check" type="checkbox" disabled="disabled" name="shift[]" value="' + shifts[k].GetID() + '"><div class = "maxmin">Full</div>');
						}
						else
						{
							print('<input class="check" type="checkbox"  name="shift[]" value="' + shifts[k].GetID() + '"><div class = "maxmin">'+shifts[k].GetEnrollCount()+'/'+shifts[k].GetMaxEnrollCount()+'</div>');
						}
						havePlacedCheckBox = true;
						break;
					}
				}
				if(!havePlacedCheckBox)
				{
					print('<div style="height:76px;width:55px;"/>');
				}
				print('</div></td>'); 
			}

			print('</tr>');

			lastDate = shiftTimes[i].GetDate();



		}
	}
	private function buildEnd()
	{
		print('<tr>'+
			'<td colspan="'+(locationLength+1)+'">'+
				'<div class="formElement" style="margin-left:130px">'+
					'<input  type="submit" name="submit" value="Submit">'+
					'<input type="reset" name="reset">'+
				'</div>'+
			'</td>'+
		'</tr>'+
		'</form>'+
		'</div>'+
	'</body>');
	}
}