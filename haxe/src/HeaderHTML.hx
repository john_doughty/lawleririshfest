package ;
import php.Lib.print;
/**
 * ...
 * @author John Doughty
 */
class HeaderHTML 
{

	public function new() 
	{
		print('<html>'+
				'<head>'+
					'<link rel="stylesheet" type="text/css" href="main.css">'+
						'<link href="http://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet" type="text/css">'+
						'<link href="http://fonts.googleapis.com/css?family=Sorts+Mill+Goudy:400,400italic" rel="stylesheet" type="text/css">'+
						'<link href="http://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css">'+
					'</head>' +
				'<body>'+
				'<div id="header">'+
					'<div id="header-content">'+
						'<img src="http://www.lawleririshfest.com/wp-content/themes/basic/themify/img.php?src=http://www.lawleririshfest.com/wp-content/uploads/2014/04/logo.png&amp;w=216&amp;h=143&amp;zc=1" alt="Lawler Irish Festival" width="216" height="143" style="margin-top:22px;">'+
					'</div>'+
				'</div>');
	}
	
}