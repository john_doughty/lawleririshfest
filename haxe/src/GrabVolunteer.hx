package ;

/**
 * ...
 * @author John Doughty
 */
class GrabVolunteer extends Volunteer
{

	private var startTime:Date;
	private var endTime:Date;
	
	public function new(firstName:String,lastName:String,email:String,phone:String,location:String,startTime:Int,endTime:Int) 
	{
		var hour:Int;
		
		super(firstName, lastName, email, phone, location);
		
		this.startTime = untyped __call__('date','g:i jS', startTime);
		this.endTime = untyped __call__('date','g:i jS', endTime);
		
	}
	
	public function GetStartTime()
	{
		return startTime;
	}

	public function GetEndTime()
	{
		return endTime;
	}
}