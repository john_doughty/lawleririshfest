<html>
	<head>
		<link rel="stylesheet" type="text/css" href="main.css">
	</head>
 	<body>
	<?php

		// Connect to Database 
		include 'main.php';
		$main = new Main;
		$connection = $main->ConnectionToDatabase();
		$volunteerIDResource= $connection->query
		(
			"SELECT 
			COUNT(id) as count 
			FROM volunteer"
		);
		foreach ($volunteerIDResource as $row) {
			$volunteerID = $row['count'];
		}
		// $query = "INSERT INTO volunteer 
		// (
		// 	id,
		// 	first_name,
		// 	last_name,
		// 	email,
		// 	phone
		// )
		// VALUES 
		// (
		// 	".$volunteerID.",'
		// 	".$_POST['firstName']."','
		// 	".$_POST['lastName']."','
		// 	".$_POST['eMail']."',
		// 	".$_POST['number']."
		// );";

			$noNumber = false;
			$noFirstName = false;
			$noLastName = false;
			$noeMail = false;
		if($_POST['number']==='')
		{
			$noNumber = true;
		}

		if($_POST['firstName']==='')
		{
			$noFirstName = true;
		}

		if($_POST['lastName']==='')
		{
			$noLastName = true;
		}

		if($_POST['eMail']==='')
		{
			$noeMail = true;
		}

		if($noeMail || $noLastName ||$noFirstName ||$noNumber||count($_POST['shift'])==0)
		{
			echo('
			<script type="text/javascript">

				window.location.replace("http://www.lawleririshfest.com/form.php?number='.$noNumber.
					'&firstName='.$noFirstName.
				'&lastName='.$noLastName.
				'&eMail='.$noeMail.'");
			</script>
			');
			die();
		}

		$query = $connection->prepare(
			"INSERT INTO volunteer
			(
				id,
				first_name,
				last_name,
				email,
				phone
			)
			VALUES
			(
				:id,
				:first_name,
				:last_name,
				:email,
				:phone
			)"
		);
		$_POST['number']=str_replace("(","",$_POST['number']);
		$_POST['number']=str_replace(")","",$_POST['number']);
		$_POST['number']=str_replace("-","",$_POST['number']);
		//taken from http://stackoverflow.com/questions/6040367/php-adding-characters-in-middle-of-a-string-starting-from-the-end
		$_POST['number']=preg_replace('/^(.*?)(.{3})(.{4})$/', '$1-$2-$3', $_POST['number']);
		$query->bindParam(":id",$volunteerID);
		$query->bindParam(":first_name",$_POST['firstName']);
		$query->bindParam(":last_name",$_POST['lastName']);
		$query->bindParam(":email",$_POST['eMail']);
		$query->bindParam(":phone",$_POST['number']);
		// Print("id ".$volunteerID);
		// Print("id ".$_POST['firstName']);
		// Print("id ".$_POST['lastName']);
		// Print("id ".$_POST['eMail']);
		// Print("id ".$_POST['number']);
		// print_r($_POST);
		// $connection->query($query);
		$query->execute();

		$query = $connection->prepare(
			"INSERT INTO volunteer_obligation_list 
			(
				vid,
				sid
			) 
			VALUES 
			(
				:vid,
				:sid
			);"
		);

		// $firstEntry = true;
		$query->bindParam(":vid",$volunteerID);
		$shiftCount=0;
		foreach ($_POST['shift'] as $row) 
		{
			$query->bindParam(":sid",$row);

			if(isset($ids))
				$ids=$ids.",";
			$ids=$ids.$row;
			$query->execute();
			$shiftCount++;
		}
		// $connection->query($query);



	 	$qString="UPDATE shift SET
				volunteers = volunteers + 1
				WHERE 
				id IN (".$ids.");";
		$query = $connection->prepare(
				$qString
			);

		
		$ShiftResource= $connection->query
		(
			"SELECT l.name as location,UNIX_TIMESTAMP(st.start_time) as start_time,UNIX_TIMESTAMP(st.start_time) as end_time
				FROM shift s
				JOIN shift_time st ON st.id = s.shift_time_id
				JOIN locations l ON l.id = s.location_id
				WHERE s.id IN (".$ids.") ORDER BY s.shift_time_id;"
		);
		$i=0;
		foreach ($ShiftResource as $row) {
			$shiftsSignedUpFor[$i]->name =  $row['location'];
			$shiftsSignedUpFor[$i]->startTime  =  date('g:i M j, Y',$row['start_time']);
			$shiftsSignedUpFor[$i]->endTime  =  date('g:i M j, Y', $row['end_time']);
			$i++;
		}
		// print_r($shiftsSignedUpFor);
		$emailString;
		for($i = 0;$i<$shiftCount;$i++) {
			$emailString=$emailString."\n".$shiftsSignedUpFor[$i]->name." at ".$shiftsSignedUpFor[$i]->startTime;
		}
		print($emailString);
	?>
	<div id='header'>

		<div id='header-content'>

			<img src="http://www.lawleririshfest.com/wp-content/themes/basic/themify/img.php?src=http://www.lawleririshfest.com/wp-content/uploads/2014/04/logo.png&amp;w=216&amp;h=143&amp;zc=1" alt="Lawler Irish Festival" width="216" height="143" style="margin-top:22px;">

		</div>

	</div>

		<div id='main-content'>

			<div id="title">

			Thanks for Volunteering!<br/>
			We'll see you there!

			</div>
			<div>
			<br/>
			<br/>
			<br/>
			Check your email for a confirmation message!
			<br/>
			If you don't see it check your spam.
			<br/>
			Please wait while we redirect you back to our main page...

		<?php
		// Print($qString);
		$query->execute();
		?>
			</div>
			<script type="text/javascript">

				setTimeout(function(){window.location.replace("http://www.lawleririshfest.com")},3000);

			</script>

		</div>
		<?php
		$to = "irishfestvolunteers@gmail.com";
	 	$headers = "From: no-reply@lawleririshfest.com";
	 	$subject = $_POST['firstName']." ".$_POST['lastName']." has volunteered";
	 	$body = $_POST['firstName']." ".$_POST['lastName']." has volunteered for ".$shiftCount." positions.
	 	\nThese Positions are:".$emailString." 
	 	\n\nFor more info, copy this into your address bar: lawleririshfest.com/grab.php?id=".$volunteerID;
	
		if (mail($to, $subject, $body,$headers)) {
		} else {
		}
		if(isset($_POST['eMail']))
		{
			if($_POST['eMail']!="")
			{
				$to = $_POST['eMail'];
			 	$headers = "From: no-reply@lawleririshfest.com";
			 	$subject = "Thank you for volunteering!";
			 	$body = "Thank you for volunteering for ".$shiftCount." positions. 
	 			\nThese Positions are:".$emailString." 
			 	\n\nThank you";
				
				if (mail($to, $subject, $body,$headers)) {
				} else {
				}
			}
		}
		?>
	</body>
</html>