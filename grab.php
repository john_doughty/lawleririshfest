<html>
	<head>
		<link rel="stylesheet" type="text/css" href="main.css">
		<style>
		td
		{
			width:120px;
		}
		</style>
	</head>
	<body>
		<div id='header'>
			<div id='header-content'>
				<img src="http://www.lawleririshfest.com/wp-content/themes/basic/themify/img.php?src=http://www.lawleririshfest.com/wp-content/uploads/2014/04/logo.png&amp;w=216&amp;h=143&amp;zc=1" alt="Lawler Irish Festival" width="216" height="143" style="margin-top:22px;">
			</div>
		</div>

		<div id='main-content'>
		<?php
			// Connect to Database 
			include 'main.php';
			$first = false;
			$last = false;
			$email = false;
			$number = false;
			$location = false;
			$searchingByID = false;


			if(isset($_GET["id"]))
			{
				if($_GET['id']!='')
				{
					//Print("first name ".$_POST['firstName']);
					$searchingByID = true;
				}
			}
			if(isset($_POST['firstName']))
			{
				if($_POST['firstName']!='')
				{
					//Print("first name ".$_POST['firstName']);
					$first = true;
				}
			}
			if(isset($_POST['lastName']))
			{
				if($_POST['lastName']!='')
				{
					//Print("last name ".$_POST['lastName']);
					$last = true;
				}
			}
			
			if(isset($_POST['eMail']))
			{
				if($_POST['eMail']!='')
				{
					//Print("email ".$_POST['eMail']);
					$email = true;
				}
			}
			
			if(isset($_POST['number']))
			{
				if($_POST['number']!='')
				{
					//Print("number ".$_POST['number']);
					$number = true;
				}
			}

			if(isset($_POST['location']))
			{
				if($_POST['location']!='')
				{
					//Print("location ".$_POST['location']);
					$location = true;
				}
			}

			$main = new Main;

			$connection = $main->ConnectionToDatabase();
			$queryString = "SELECT DISTINCT v.first_name, v.last_name,v.email,
			 v.phone, l.name as location,
			 UNIX_TIMESTAMP(st.start_time) as start_time,
			 UNIX_TIMESTAMP(st.end_time) as end_time 
				FROM volunteer v
				JOIN volunteer_obligation_list vl ON v.id = vl.vid
				JOIN shift s ON s.id = vl.sid
				JOIN shift_time st ON st.id = s.shift_time_id
				JOIN locations l ON l.id = s.location_id";

			if($first||$last||$email||$number||$location||$searchingByID)
			{
				$queryString = $queryString." WHERE ";
			}

			if($first)
			{
				$queryString = $queryString."v.first_name = '".$_POST['firstName']."'" ;
			}

			if($last)
			{
				if($first)
					$queryString = $queryString." AND ";
				$queryString = $queryString."v.last_name = '".$_POST['lastName']."'" ;
			}
			
			if($email)
			{
				if($first||$last)
					$queryString = $queryString." AND ";
				$queryString = $queryString."v.email = '".$_POST['eMail']."'" ;
			}
			
			if($number)
			{
				if($first||$last||$email)
					$queryString = $queryString." AND ";
				$queryString = $queryString."v.phone = '".$_POST['number']."'" ;
			}

			if($location)
			{
				if($first||$last||$email||$number)
					$queryString = $queryString." AND ";
				$queryString = $queryString."l.name = '".$_POST['location']."'" ;
			}

			if($searchingByID)
			{
				if($first||$last||$email||$number||$location)
					$queryString = $queryString." AND ";
				$queryString = $queryString."v.id = '".$_GET['id']."'" ;
			}

			$queryString = $queryString." ORDER BY st.start_time,l.name,v.last_name,v.first_name ASC";
			//Print($queryString);

			$data = $connection -> query($queryString);

			$volunteers  = array();
			$i=0;

			foreach ($data as $key => $value) 
			{
				$volunteers[$i] = new GrabVolunteer
				(
					$value['first_name'],
					$value['last_name'],
					$value['email'],
					$value['phone'],
					$value['location'],
					$value['start_time'],
					$value['end_time']
				);
				$i++;
			}
			$volunteersLength = $i;

			$data = $connection -> query('SELECT name FROM locations');
			
			$i = 0;
			$locations = array();
			foreach ($data as $key => $value) 
			{
				
				$locations[$i] = $value['name'];
				$i++;
			}
			$locationsLength = $i;

			Print('<form id="grabForm" action ="grab.php" method = "post">');
 			Print('<table cellpadding="3">');
			Print('<tr>');
				Print('<td >');
					Print("First Name");
				Print('</td>');
				Print('<td>');
					Print("Last Name");
				Print('</td>');
				Print('<td>');
					Print("Email Address");
				Print('</td>');
				Print('<td>');
					Print("Phone Number");
				Print('</td>');
				Print('<td>');
					Print("Start Time");
				Print('</td>');
				Print('<td>');
					Print("End Time");
				Print('</td>');
				Print('<td>');
					Print("Location");
				Print('</td>');
			Print('</tr>');
			Print('<tr>');
				Print('<td >');
					Print('<input type="text" name="firstName">');
				Print('</td>');
				Print('<td>');
					Print('<input type="text" name="lastName">');
				Print('</td>');
				Print('<td>');
					Print('<input type="text" name="eMail">');
				Print('</td>');
				Print('<td>');
					Print('<input type="text" name="number">');
				Print('</td>');
				Print('<td/>');
				Print('<td/>');
				Print('<td>');

					for($i = 0; $i<$locationsLength; $i++)
					{
						Print('<br/><input type="radio" name="location" value="'.$locations[$i].'">'.$locations[$i]);
					}
				Print('</td>');
				Print('<td>');
					Print('<input style = "height: 25px;width:50px" type="submit" value="Filter">');
				Print('</td>');
			Print('</tr>');
			$bDark=true;
			for($i=0;$i<$volunteersLength;$i++) 
			{
				print ("<tr ".($bDark?"class='darkRow'":"class='lightRow'").">
					<td>".$volunteers[$i]->GetFirstName()."</td>
					<td>".$volunteers[$i]->GetLastName()."</td>
					<td>".$volunteers[$i]->GetEmailName()."</td>
					<td>".$volunteers[$i]->GetPhone()."</td>
					<td>".$volunteers[$i]->GetStartTime()."</td>
					<td>".$volunteers[$i]->GetEndTime()."</td>
					<td>".$volunteers[$i]->GetLocation()."</td>
					</tr>");
				$bDark?$bDark=false:$bDark=true;
			}
			Print('</table>');
			Print("</form>");

		?>
		</div>
	</body>
</html>