<html>

	<head>

		<link rel="stylesheet" type="text/css" href="main.css">

		<link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>

		<link href='http://fonts.googleapis.com/css?family=Sorts+Mill+Goudy:400,400italic' rel='stylesheet' type='text/css'>

		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>

		<?php

			// Connect to Database 

			include 'main.php';
			$noNumber = false;
			$noFirstName = false;
			$noLastName = false;
			$noeMail = false;

			if(isset($_GET["firstName"]))
			{
				if($_GET["firstName"]==true)
				{
					$noFirstName = true;
				}
			}
			if(isset($_GET["lastName"]))
			{
				if($_GET["lastName"]==true)
				{
					$noLastName = true;
				}
			}
			if(isset($_GET["number"]))
			{
				if($_GET["number"]==true)
				{
					$noNumber = true;
				}
			}
			if(isset($_GET["eMail"]))
			{
				if($_GET["eMail"]==true)
				{
					$noeMail = true;
				}
			}

			$main = new Main;



			$connection = $main->ConnectionToDatabase();

 			$locationsdata = $connection->query("SELECT l.name as location,s.id,s.shift_time_id,
(SELECT COUNT(*) FROM volunteer_obligation_list WHERE sid = s.id) as volunteers,

s.max_volunteers,UNIX_TIMESTAMP(st.start_time) as start_time,UNIX_TIMESTAMP(st.end_time) as end_time

				FROM shift s

				JOIN shift_time st ON st.id = s.shift_time_id

				JOIN locations l ON l.id = s.location_id

				LEFT JOIN volunteer_obligation_list v ON s.id = v.sid

				ORDER BY st.start_time;");  

 			// foreach ($locationsdata as $key => $value) {

 			// 	print_r($value);

 			// 	print("<br/>");

 			// }

 			$shifts = array();

 			$shiftsLength=0;

 			foreach ($locationsdata as $key => $value) {

 				$shifts[$shiftsLength] = new Shift($value['location'],$value['id'],$value['shift_time_id'],$value['volunteers'],$value['max_volunteers'],$value['start_time'],$value['end_time']);

 				$shiftsLength++;

 			}



 			$locations = array();

 			$locationLength = 0;

 			$ShiftTimes = array();

 			$ShiftTimeLength = 0;



 			for($i=0;$i<$shiftsLength;$i++)

 			{

 				$dupeLocation = false;

 				$dupeShiftTime = false;



 				for($j=0;$j<$locationLength;$j++)

 				{

 					if($locations[$j]===$shifts[$i]->GetLocationName())

 					{

 						$dupeLocation = true;

 						break;

 					}

 				}



 				for($j=0;$j<$ShiftTimeLength;$j++)

 				{

 					if($ShiftTimes[$j]->GetID()===$shifts[$i]->GetTimeID())

 					{

 						$dupeShiftTime = true;

 						break;

 					}

 				}

 				

 				if ($dupeLocation===false)

 				{

	 				$locations[$locationLength] = $shifts[$i]->GetLocationName();

	 				$locationLength++;

 				}

 				

 				if ($dupeShiftTime===false)

 				{

	 				$ShiftTimes[$ShiftTimeLength] = new ShiftTime($shifts[$i]->GetTimeID(), $shifts[$i]->GetStartTime(),$shifts[$i]->GetEndTime(), $shifts[$i]->GetDate(), $shifts[$i]->GetDayOfTheWeek());

	 				$ShiftTimeLength++;

 				}

 			}
		?>

	</head>

	<body>

	<div id='header'>

		<div id='header-content'>

			<img src="http://www.lawleririshfest.com/wp-content/themes/basic/themify/img.php?src=http://www.lawleririshfest.com/wp-content/uploads/2014/04/logo.png&amp;w=216&amp;h=143&amp;zc=1" alt="Lawler Irish Festival" width="216" height="143" style="margin-top:22px;">

		</div>

	</div>

		<div id='main-content'>

		<div id="title">

		Lawler Irish Festival <br/>Volunteer Form

		</div>

		<form id="volunteerForm" action ="Submit.php" method = "post">

		<table>

			<div class=\"formElement\">

				<tr>

					<!--start date end date will go here-->

				</tr>

				<tr>

					<td> <div class="column1">First Name</div> </td>

					<td colspan=<?php echo("'".$locationLength."'")?> style="vertical-align: middle;"> <div ><input type="text" 
						<?php if($noFirstName) echo('style="border: 5px solid red;"');?>
					name="firstName"></div><td>

				</tr>

				<tr>

					<td> <div class="column1">Last Name</div> </td>

					<td colspan=<?php echo("'".$locationLength."'")?> style="vertical-align: middle;"> <div ><input type="text" 
						<?php if($noLastName) echo('style="border: 5px solid red;"');?>
					name="lastName"></div><td>

				</tr>	

				<tr>

					<td> <div class="column1">Email Address</div> </td>

					<td colspan=<?php echo("'".$locationLength."'")?> style="vertical-align: middle;"> <div ><input type="text" 
						<?php if($noeMail) echo('style="border: 5px solid red;"');?>
					name="eMail"></div><td>

				</tr>				

				<tr>

					<td> <div class="column1">Phone Number</div> </td>
						
					<td colspan=<?php echo("'".$locationLength."'")?> style="vertical-align: middle;"> <div><input type="text" 
						<?php if($noNumber) echo('style="border: 5px solid red;"');?>
					name="number"></div><td>

				</tr>			

				<tr>
					<td/>
					<td colspan=<?php echo("'".($locationLength)."'")?> style="vertical-align: middle;"> <div>NOTE: The two numbers in each column (0/1) represent the number of people who have volunteered, followed by the number of volunteers needed for each position. A position with a * requires being 21 years of age.</div><td>

				</tr>	

			</div>

		<div class=\"formElement\">

		<div class=\"checkArea\">



		

		<?php

		$dayoftheweek = "";

		$lastDate = "";

		for($i=0;$i < $ShiftTimeLength;$i++)

		{

		

			if($ShiftTimes[$i]->GetDate() != $lastDate)

			{

				Print("<tr>");

				Print("<td/>");

			$bDark=true;
				for($l = 0; $l < $locationLength; $l++)

				{
					Print("<td ".($bDark?"style = 'background-color: #a4b193;'":"")."><div class='checkHeader' >");
				  	$bDark?$bDark=false:$bDark=true;
				  	Print($locations[$l]."</div></td>"); 

				}

				Print("</tr>");

				Print('<tr>');
					Print('<td class="day">'.$ShiftTimes[$i]->GetDayOfTheWeek().'</td>');
					for($l = 0; $l < $locationLength; $l++)
					{
						Print("<td ".($bDark?"style = 'background-color: #a4b193;height:32px;'":"")."><div class='checkHeader' >");
					  	$bDark?$bDark=false:$bDark=true;
					  	Print("</div></td>"); 

					}
				Print('</tr>');

			}

			Print("<tr>");

			Print("<td><div class=\"time\">".$ShiftTimes[$i]->GetStartTime()." - ".$ShiftTimes[$i]->GetEndTime()."</div></td>");

			$bDark=true;
			for($j = 0; $j < $locationLength; $j++)

			{

			  	Print("<td><div ".($bDark?"class='darkColumn'":"class='lightColumn'").">");
			  	$bDark?$bDark=false:$bDark=true;
			  	$havePlacedCheckBox = false;

				for($k = 0; $k < $shiftsLength;$k++)

				{

					if($shifts[$k]->GetLocationName()===$locations[$j] && 

						$shifts[$k]->GetTimeID()===$ShiftTimes[$i]->GetID())

					{
						if($shifts[$k]->GetEnrollCount() >=$shifts[$k]->GetMaxEnrollCount())

						{

						Print("<input class=\"check\" type=\"checkbox\" disabled='disabled' name=\"shift[]\" value=\"".$shifts[$k]->GetID()."\"><div class = \"maxmin\">Full</div>");

						}

						else

						{

						Print("<input class=\"check\" type=\"checkbox\" name=\"shift[]\" value=\"".$shifts[$k]->GetID()."\"><div class = \"maxmin\">".$shifts[$k]->GetEnrollCount()."/".$shifts[$k]->GetMaxEnrollCount()."</div>");

						}

						//Print($shifts[$k]->GetLocationName()." ".$shifts[$k]->GetTimeID(). " ".$k);

						$havePlacedCheckBox = true;

						break;

					}

				}

				if(!$havePlacedCheckBox)

				{

					Print("<div style='height:76px;width:55px;'/>");

				}

			  	Print("</div></td>"); 

			}

			Print("</tr>");

			$lastDate = $ShiftTimes[$i]->GetDate();



		}

		?>

		<tr>

			<td colspan=<?php echo("'".($locationLength+1)."'")?>>

				<div class="formElement" style="margin-left:130px">

					<input  type="submit" name="submit" value="Submit">

					<input type="reset" name="reset">

				</div>

			</td>

		</tr>

		</form>

		</div>

	</body>